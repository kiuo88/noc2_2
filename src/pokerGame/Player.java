package pokerGame;

/*
 *  H�rs till Moment A.
 */
public class Player {
	String lastname, club;
	int token;
	int round;
	int currentRound; // f�r att g�ra Moment B och F l�ttare

	public Player(String xx, String yy) {
		lastname = xx;
		club = yy;
		token = 100;
		round = 0;
		currentRound = 0; // f�r att g�ra Moment B och F l�ttare
	}
	public Player(String xx, String yy, int zz, int ii, int ll) { //ladda filen i Moment B
		lastname = xx;
		club = yy;
		token = zz;
		round = ii;
		currentRound = ll;
	}

	public void roundLost(int aa) {
		round = aa;
	}
	public void currentRound(int bb){//hj�lpare till B och F
		currentRound = bb;
	}

	public int getCurrentRound(){//hj�lpare fill B
		return currentRound;
	}
	
	public int getRoundLost() {
		return round;
	}

	public void changeToken(int zz) {
		token = token + zz;
	}

	public String getLast() {
		return lastname;
	}

	public String getClub() {
		return club;
	}

	public int getToken() {
		return token;
	}
}
