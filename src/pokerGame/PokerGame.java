package pokerGame;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PokerGame {
	BufferedReader in = null; 
    JFileChooser chooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("Save File (txt)", "txt");
    boolean testNewGame;
	private static String fileName =new SimpleDateFormat("yyyyMMddhhmm'.txt'").format(new Date()); 
	boolean alltokens;
	public void startGame() { // Denna method tillh�r moment A.
		
		int n = JOptionPane.showConfirmDialog(null, "Is this a new game?", "Program start",
				JOptionPane.YES_NO_CANCEL_OPTION);
		switch (n) {
		case 0:
			testNewGame = true;
			this.nOngoingGame();
			break; // moment c
		case 1:
			testNewGame = false;
			this.nOngoingGame();
			break; // moment B
		case 2:
			System.exit(0);
		}
	}

	public Player[] newGame() { // Denna method tillh�r moment A.
		alltokens = false;
		Object[] aPlayers = { "2", "3", "4", "5", "6", "7", "8", "9", "10" };
		String n = (String) JOptionPane.showInputDialog(null, "Choose the amount of players: ", "New Game",
				JOptionPane.QUESTION_MESSAGE, null, aPlayers, "2");
		if (n == null) {
			System.out.println("Goodbye!");
			System.exit(0);
		}
		Player[] p = new Player[Integer.parseInt(n)];
		for (int i = 0; i < p.length; i++) {
			JTextField input1 = new JTextField();
			JTextField input2 = new JTextField();
			Object[] message = { "Lastname:", input1, "Club:", input2 };
			int ulfdg = i + 1;
			int option = JOptionPane.showConfirmDialog(null, message, "Input player " + ulfdg,
					JOptionPane.OK_CANCEL_OPTION);
			if (option == 2 || option < 0) {
				System.exit(0);
			}
			if (input1.getText().length() == 0) {
				JOptionPane.showMessageDialog(null, "Please insert player name");
				i--;
			} else if (input2.getText().length() == 0) {
				JOptionPane.showMessageDialog(null, "Please insert club name");
				i--;
			} else {
				p[i] = new Player(input1.getText(), input2.getText());
			}
		}
		return p;
	}
public Player[] loadGame() { //moment B
	int length = 0;
	String playerIn = null;
	boolean fileNotSelected = true;
	while (fileNotSelected) {
	chooser.setFileFilter(filter);
    int returnVal = chooser.showOpenDialog(null);
    if(returnVal == JFileChooser.APPROVE_OPTION) {
    	LineNumberReader lnr = null;
		try {
			lnr = new LineNumberReader(new FileReader(new File(chooser.getSelectedFile().getName())));
		} catch (FileNotFoundException e) {
			continue;
		}
    	try {
			lnr.skip(Long.MAX_VALUE);
		} catch (IOException e) {
			continue;
		}

    	length = lnr.getLineNumber();

    	try {
			lnr.close();
		} catch (IOException e) {
			continue;
		}
    	try {
			in = new BufferedReader(new FileReader(chooser.getSelectedFile().getName()));
		} catch (FileNotFoundException e) {
			continue;
		}
    }
    fileNotSelected = false;
	}	
	Player[] p = new Player[length];
	for (int i = 0; i < length; i++){
		try {
			playerIn = in.readLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Something went wrong");
			System.exit(0);
		}
			String[] playerThings = playerIn.split(",");
			String lastname = playerThings[0];
			String club = playerThings[1];
			int token = Integer.parseInt(playerThings[2]);
			int round = Integer.parseInt(playerThings[3]);
			int currentRound = Integer.parseInt(playerThings[4]);
			p[i] = new Player(lastname,club,token,round,currentRound);
	}


	return p;
}
	public void nOngoingGame() { // moment C
		Player[] p2 = null;
		if (testNewGame){ //moment a
		p2 = newGame();
		}
		if (!testNewGame){ //moment b
		p2 = loadGame();
		}
		int tokenN = 0;
		int r = p2[0].getCurrentRound(); //hj�lper moment b
		
		while (!alltokens) { //en while �r en runda av spelet
			r++;
			ArrayList<Player> tempIngame = new ArrayList<Player>();
			boolean fail = false;
			int[] tempP2 = new int[p2.length];
			int checkSumP2 = 0;
			int positivityTest = 0;
			for (int i = 0; i < p2.length; i++) { // en for �r en spelares tur 
				p2[i].currentRound(r);
				if ((p2[i].getRoundLost()) == 0) {
					String tokenS = JOptionPane.showInputDialog(null,
							"<html>It is now Round " + r + "<br>Please insert tokens won/lost for player<br>"
									+ p2[i].getLast() + ", " + p2[i].getClub() + "<br>(current tokens available: " + p2[i].getToken() + ")</html>");
					if (tokenS == null) {
						int n = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit?", "Quitting?",
								JOptionPane.YES_NO_OPTION);
						switch (n) {
						case 0:
							System.exit(0);
							;
						case 1:
							i--;
							break;
						}
					} else {
						try { // moment D
							tokenN = Integer.parseInt(tokenS);
						} catch (NumberFormatException ex) {
							JOptionPane.showMessageDialog(null, "Please insert a number");
							i--;
							continue;

						}
						if ((p2[i].getToken() + tokenN) < 0) { //moment E
							JOptionPane.showMessageDialog(null, "You can't lose this many tokens");
							i--;
						} else if (tokenN == 0) { // man m�ste delta i varje runda
							JOptionPane.showMessageDialog(null, "You must play");
							i--;																							
				}  else {
							checkSumP2 = checkSumP2 + tokenN;
							tempP2[i] = tokenN;
							if (tokenN > 0) { 
								positivityTest++;
							}
						}
					}
				}
			}

			if (checkSumP2 != 0) {
				JOptionPane.showMessageDialog(null, "Tokens didn't add up!"); //moment  e
				r--;
				fail =true;
			} else if (positivityTest != 1) {
				JOptionPane.showMessageDialog(null, "Only one winner per round."); //moment e
				r--;
				fail=true;
			} else {
				for (int u = 0; u < p2.length; u++) { //kollar om n�n �r utan spelm�rken och tar upp vilken runda det �r

					p2[u].changeToken(tempP2[u]);

					if (p2[u].getToken() == 0 && p2[u].getRoundLost() == 0) {
						JOptionPane.showMessageDialog(null, "Player " + p2[u].getLast() + " lost!");
						p2[u].roundLost(r);

					}
				}
				
				for (int f = 0; f < p2.length; f++) { //
					if (p2[f].getToken() == 100 * p2.length) { //kollar om en spelare har alla spelm�rken, om jo s� kan man b�rja ett nytt spel, om inte g�r den framm�t
						alltokens = true;
						int n = JOptionPane.showConfirmDialog(null,
								p2[f].getLast() + " representing " + p2[f].getClub() + " won \nNew game?", "Winner!!",
								JOptionPane.YES_NO_OPTION);
						switch (n) {
						case 0:
							main(null);
							break;
						case 1:
							System.exit(0);
							break;
			}}
			if (!fail) {
				UIManager.put("OptionPane.cancelButtonText", "Save Session");
				int n55 = JOptionPane.showConfirmDialog(null, "Do you want to see the current situation ?", //moment g of f
						"End of Round", JOptionPane.YES_NO_CANCEL_OPTION);
				UIManager.put("OptionPane.cancelButtonText", "Quit");
				switch (n55) {
				case 0:fail=true; // moment f
					//Player [] p3 = ;
					StringBuilder sb = new StringBuilder();
					sb.append("Current Situation is: \n");
					for (int e1 = 0; e1 < p2.length; e1++) {
						if (p2[e1].getToken() != 0) {
							tempIngame.add(p2[e1]);
							for(Player s : tempIngame){
							sb.append(s.getLast()+" representing " +s.getClub()+" has "+s.getToken());
							sb.append("\n");
							}

						}
					}
					
					sb.append("Theese persons have lost so far: \n");
					for (int e2 = 0; e2 < p2.length; e2++) {
						if (p2[e2].getToken() == 0) {
							sb.append(p2[e2].getLast() + " representing " + p2[e2].getClub() + " has lost on round "
									+ p2[e2].getRoundLost() + "\n");
						}
					}
					JOptionPane.showMessageDialog(null, sb.toString());
					break;
				case 1:fail=true;
					break;
				case 2:fail=true; //moment G
					for (int ii = 0 ; ii < p2.length; ii++){
					try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true))) {
						String content = p2[ii].getLast() + "," + p2[ii].getClub()  + "," + p2[ii].getToken() + "," + p2[ii].getRoundLost() + "," + p2[ii].getCurrentRound();
						bw.write(content);
						bw.write(System.lineSeparator());
						bw.close();
					} catch (IOException e) {
					e.printStackTrace();
				}}
				case -1:
					int m = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit?", "Quitting?",
							JOptionPane.YES_NO_OPTION);
					switch (m) {
					case 0:
						System.exit(0);
						;
					case 1:
						break;
					case -1:
						System.exit(0);
					}
				}
				
			}

			
					}
				}
			}
		}

	public static void main(String[] args) { // Denna method tillh�r moment A.
		UIManager.put("OptionPane.cancelButtonText", "Quit");
		UIManager.put("OptionPane.okButtonText", "Continue");
		PokerGame newPGame = new PokerGame();
		newPGame.startGame();
	}

}
